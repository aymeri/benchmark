/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package at.ymeri.banchmark;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

import at.ymeri.loops.CommonMultiple;
import at.ymeri.loops.ParallelizableOperations;
import at.ymeri.model.User;
import at.ymeri.repository.UserRepository;

public class MyBenchmark {
	
	@State(Scope.Thread)
	public static class Users {
		public List<User> generateUsers() {
			UserRepository userRepo = new UserRepository();
			return userRepo.generateUsers(1000);
		}

		public ZonedDateTime getStartDate() {
			return ZonedDateTime.now().minusYears(18);
		}

		public ParallelizableOperations getParallelizableOperations() {
			return new ParallelizableOperations();
		}
	}
	
	/*
	 * 
	@Benchmark@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testLoops() {
		CommonMultiple ui = new CommonMultiple();
		ui.findCommonMultiplesUsingLoops(2, 3, 100000);
	}

	@Benchmark@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testStreams() {
		CommonMultiple ui = new CommonMultiple();
		ui.findCommonMultiplesUsingStream(2, 3, 100000);

	}
	 */
	
	@Benchmark@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testLoops(Users state) {
		ParallelizableOperations po = state.getParallelizableOperations();
		List<User> filteredUsers =  po.findByBirthdayRange_loops(state.generateUsers(), state.getStartDate());
		if(filteredUsers.size() > 500) {
			System.out.println(filteredUsers.size());
		}

	}
	
	@Benchmark@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testSequentialStreams(Users state) {
		ParallelizableOperations po = state.getParallelizableOperations();
		List<User> filteredUsers =  po.findByBirthdayRange_streams(state.generateUsers(), state.getStartDate());
		if(filteredUsers.size() > 500) {
			System.out.println(filteredUsers.size());
		}

	}


	@Benchmark@BenchmarkMode(Mode.AverageTime)
	@OutputTimeUnit(TimeUnit.MILLISECONDS)
	public void testParallelStreams(Users state) {
		ParallelizableOperations po = state.getParallelizableOperations();
		List<User> filteredUsers =  po.findByBirthdayRange_parallelStreams(state.generateUsers(), state.getStartDate());
		if(filteredUsers.size() > 500) {
			System.out.println(filteredUsers.size());
		}

	}

}
