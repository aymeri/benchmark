package at.ymeri.repository;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import at.ymeri.model.User;

@SuppressWarnings("nls")
public class UserRepository {

	private final List<User> basket = Collections
		.unmodifiableList(Arrays.asList(
				new User("user1", "user1@mail.at", "First Name 1", "Last Name 1", ZonedDateTime.now()
					.minusYears(12)),
				new User("user2", "user2@mail.at", "First Name 2", "Last Name 2", ZonedDateTime.now()
					.minusYears(12)),
				new User("user3", "user3@mail.at", "First Name 3", "Last Name 3", ZonedDateTime.now()
					.minusYears(14)),
				new User("user4", "user4@mail.at", "First Name 4", "Last Name 4", ZonedDateTime.now()
					.minusYears(14)),
				new User("user5", "user5@mail.at", "First Name 5", "Last Name 5", ZonedDateTime.now()
					.minusYears(18)),
				new User("user6", "user6@mail.at", "First Name 6", "Last Name 6", ZonedDateTime.now()
					.minusYears(19)),
				new User("user7", "user7@mail.at", "First Name 7", "Last Name 7", ZonedDateTime.now()
					.minusYears(20)),
				new User("user8", "user8@mail.at", "First Name 8", "Last Name 8", ZonedDateTime.now()
					.minusYears(20)),
				new User("user9", "user9@mail.at", "First Name 9", "Last Name 9", ZonedDateTime.now()
					.minusYears(21)),
				new User("user10", "user10@mail.at", "First Name 10", "Last Name 10", ZonedDateTime.now()
					.minusYears(21))));

	public List<User> generateUsers(int numberOfUsers) {
		List<User> users = new ArrayList<>();

		for (int i = 0; i < numberOfUsers; i++) {
			User sample = basket.get(i % 10);
			String username = addSuffix(sample.getUsername(), i);
			String email = addSuffix(sample.getEmail(), i);
			String firstname = addSuffix(sample.getFirstName(), i);
			String lastName = addSuffix(sample.getLastName(), i);
			ZonedDateTime birthday = sample.getBirthday()
				.minusDays(i);
			User user = new User(username, email, firstname, lastName, birthday);
			users.add(user);
		}

		return users;
	}

	private String addSuffix(String value, int i) {
		return value + "_" + i;
	}

}
