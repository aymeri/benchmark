package at.ymeri;

import java.time.Duration;
import java.time.Instant;

import at.ymeri.loops.CommonMultiple;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        
        CommonMultiple ui = new CommonMultiple();
        
        int a = 2;
        int b = 9;
        int max = 70000000;
        
        Instant now = Instant.now();
        ui.findCommonMultiplesUsingStream(a, b, max);
        Duration duration = Duration.ofMillis(Instant.now().toEpochMilli() - now.toEpochMilli());
        System.out.println("Duration using streams: "  + duration.toMillis() + " millis");
        
        
        now = Instant.now();
        ui.findCommonMultiplesUsingLoops(a, b, max);
        duration = Duration.ofMillis(Instant.now().toEpochMilli() - now.toEpochMilli());
        System.out.println("Duration using loops: "  + duration.toMillis() + " millis");
        
        
//        now = Instant.now();
//        ui.findCommonMultiplesUsingParallelStream(a, b, max);
//        duration = Duration.ofMillis(Instant.now().toEpochMilli() - now.toEpochMilli());
        
//        System.out.println("Duration using parallel streams: "  + duration.toMillis() + " millis");
        
    }
}
