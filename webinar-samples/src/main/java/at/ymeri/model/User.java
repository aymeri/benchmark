package at.ymeri.model;

import java.time.ZonedDateTime;

public class User {

	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private ZonedDateTime birthday;

	public User(String username, String email, String firstName, String lastName, ZonedDateTime birthday) {
		this.username = username;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthday = birthday;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public ZonedDateTime getBirthday() {
		return birthday;
	}

}
