package at.ymeri.loops;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import at.ymeri.model.User;

public class ParallelizableOperations {
	
	public List<User> findByBirthdayRange_loops(List<User> users, ZonedDateTime start) {
		List<User> result = new ArrayList<>();
		for(User user : users) {
			if(user.getBirthday().isAfter(start)) {
				result.add(user);
			}
		}
		return result;
	}
	
	public List<User> findByBirthdayRange_streams(List<User> users, ZonedDateTime start) {
		return users.stream()
				.filter(user -> user.getBirthday().isAfter(start))
				.collect(Collectors.toList());
	}
	
	public List<User> findByBirthdayRange_parallelStreams(List<User> users, ZonedDateTime start) {
		return users.parallelStream()
				.filter(user -> user.getBirthday().isAfter(start))
				.collect(Collectors.toList());
	}

}
