package at.ymeri.loops;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommonMultiple {

	public List<String> findCommonMultiplesUsingStream(int a, int b, int max) {

		return Stream.iterate(0, i -> i < max, i -> i + a)
				.filter(i -> i % b == 0)
				.map(i -> String.valueOf(i))
				.collect(Collectors.toList());

	}
	
	public List<String> findCommonMultiplesUsingParallelStream(int a, int b, int max) {

		return Stream.iterate(0, i -> i < max, i -> i + a)
				.parallel()
				.filter(i -> i % b == 0)
				.map(i -> String.valueOf(i))
				.collect(Collectors.toList());

	}
	
	
	public List<String> findCommonMultiplesUsingLoops(int a, int b, int max) {
		List<String> multiples = new ArrayList<>();
		for(int i = 0; i<max; i += a) {
			if(i %b == 0) {
				multiples.add(String.valueOf(i));
			}
			
		}
		return multiples;

	}

}
