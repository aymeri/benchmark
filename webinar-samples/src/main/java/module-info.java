module webinar.samples {
	
	requires java.base;
	
	exports at.ymeri;
	exports at.ymeri.loops;
	exports at.ymeri.model;
	exports at.ymeri.repository;
}